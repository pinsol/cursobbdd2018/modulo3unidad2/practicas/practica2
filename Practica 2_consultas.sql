﻿USE practica2;

                                             /* PRACTICA NUMERO 2 */

/* 1. Indicar el número de ciudades que hay en la tabla ciudades */

SELECT
 COUNT(*) nCiudades 
FROM
 ciudad
;

/* 2. Indicar el nombre de las ciudades que tengan una población por encima de la población media */
SELECT
 nombre 
FROM
 ciudad 
WHERE
 población>(SELECT AVG(población) FROM ciudad)
;

/* 3. Indicar el nombre de las ciudades que tengan una población por debajo de la población media */
SELECT
 nombre 
FROM
 ciudad 
WHERE
 población<(SELECT AVG(población) FROM ciudad)
;

/* 4. Indicar el nombre de la ciudad con la población máxima */
SELECT
 nombre 
FROM
 ciudad 
WHERE
 población=(SELECT MAX(población) FROM ciudad)
;

/* 5. Indicar el nombre de la ciudad con la población mínima */
SELECT
 nombre 
FROM
 ciudad 
WHERE
 población=(SELECT MIN(población) FROM ciudad)
;

/* 6. Indicar el número de ciudades que tengan una población por encima de la población media */
SELECT
 COUNT(*) 
FROM
 ciudad
WHERE
 población>(SELECT AVG(población) FROM ciudad)
;

/* 7. Indicarme el número de personas que viven en cada ciudad */
SELECT
 ciudad,COUNT(nombre) 
FROM
 persona
GROUP BY
 ciudad
;

/* 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias */
SELECT
 compañia, COUNT(persona) 
FROM
 trabaja 
GROUP BY compañia
;

/* 9. Indicarme la compañía que mas trabajadores tiene */
CREATE OR REPLACE VIEW p9c1 AS
SELECT compañia, COUNT(*) n FROM trabaja GROUP BY compañia;

CREATE OR REPLACE VIEW p9c2 AS
SELECT MAX(n) maximo FROM p9c1;


SELECT
  compañia, COUNT(persona) n 
FROM trabaja
  GROUP BY compañia
  HAVING n= (SELECT maximo FROM p9c2)
;

/* 10. Indicarme el salario medio de cada una de las compañias */
SELECT
   compañia, AVG(salario) 
FROM
   trabaja
    GROUP BY compañia
;

/* 11. Listarme el nombre de las personas y la población de la ciudad donde viven */
SELECT
   persona.nombre,población 
FROM
   persona
    JOIN ciudad ON persona.ciudad = ciudad.nombre
;

/* 12. Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive */
SELECT
   p.nombre,p.calle,población 
FROM
   persona p
    JOIN ciudad c ON p.ciudad = c.nombre
;

/* 13. Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja */
SELECT
   t.persona,
   p.ciudad vive,
   c.ciudad trabaja 
FROM
   trabaja t
    JOIN persona p ON t.persona=p.nombre
    JOIN compañia c ON t.compañia=c.nombre
;

/* 15. Listarme el nombre de la persona y el nombre de su supervisor */
SELECT persona,supervisor FROM supervisa;

/* 16. Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos */
SELECT
 s.persona,
 s.supervisor,
 p.ciudad vive,
 p1.ciudad ciudadSupervisador 
FROM supervisa s
 JOIN persona p ON s.persona=p.nombre
 JOIN persona p1 ON s.supervisor=p1.nombre
;

/* 17. Indicarme el número de ciudades distintas que hay en la tabla compañía */
SELECT COUNT(DISTINCT ciudad) FROM compañia;

/* 18. Indicarme el número de ciudades distintas que hay en la tabla personas */
SELECT COUNT(DISTINCT ciudad) FROM persona;

/* 19. Indicarme el nombre de las personas que trabajan para FAGOR */
SELECT
 persona 
FROM
 trabaja 
WHERE
 compañia LIKE 'Fagor'
;

/* 20. Indicarme el nombre de las personas que no trabajan para FAGOR */
SELECT 
 persona 
FROM
 trabaja 
WHERE 
 compañia NOT LIKE 'Fagor'
;

/* 21. Indicarme el número de personas que trabajan para INDRA */
SELECT
  COUNT(*) 
FROM
  trabaja t 
WHERE 
compañia LIKE 'Indra'
;

/* 22. Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA */
SELECT 
 persona 
FROM
 trabaja
WHERE 
 compañia LIKE 'Fagor'
  OR 
 compañia LIKE 'Indra'
;

/* 23. Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja.
Ordenar la salida por nombre de la persona y por salario de forma descendente */
SELECT
 c.población,
 t.salario,
 p.nombre,
 t.compañia 
FROM
 trabaja t
   JOIN persona p ON t.persona=p.nombre
   JOIN ciudad c ON p.ciudad=c.nombre
;